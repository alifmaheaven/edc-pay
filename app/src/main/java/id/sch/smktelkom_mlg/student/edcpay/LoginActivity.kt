package id.sch.smktelkom_mlg.student.edcpay

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.Toast
import com.alimuzaffar.lib.pin.PinEntryEditText
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        supportActionBar!!.hide()
        val pinEntry2 = findViewById<PinEntryEditText>(R.id.txt_pin_entry2)

        button0.setOnClickListener {

            pinEntry2.setText(pinEntry2.text.toString() + "0")
        }
        button1.setOnClickListener {
            pinEntry2.setText(pinEntry2.text.toString() + "1")
        }
        button2.setOnClickListener {
            pinEntry2.setText(pinEntry2.text.toString() + "2")
        }
        button3.setOnClickListener {
            pinEntry2.setText(pinEntry2.text.toString() + "3")
        }
        button4.setOnClickListener {
            pinEntry2.setText(pinEntry2.text.toString() + "4")
        }
        button5.setOnClickListener {
            pinEntry2.setText(pinEntry2.text.toString() + "5")
        }
        button6.setOnClickListener {
            pinEntry2.setText(pinEntry2.text.toString() + "6")
        }
        button7.setOnClickListener {
            pinEntry2.setText(pinEntry2.text.toString() + "7")
        }
        button8.setOnClickListener {
            pinEntry2.setText(pinEntry2.text.toString() + "8")
        }
        button9.setOnClickListener {
            pinEntry2.setText(pinEntry2.text.toString() + "9")
        }
        clearbutton.setOnClickListener {
            pinEntry2.setText("")
        }



        if (pinEntry2 != null) {
            clearbutton.visibility = VISIBLE
            pinEntry2.setAnimateText(true)
            pinEntry2.setOnPinEnteredListener { str ->
                if (str.toString() == "123456") {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    Toast.makeText(this@LoginActivity, "SUCCESS", Toast.LENGTH_SHORT).show()
                } else {
                    pinEntry2.isError = true
                    Toast.makeText(this@LoginActivity, "FAIL", Toast.LENGTH_SHORT).show()
                    pinEntry2.postDelayed({ pinEntry2.text = null }, 1000)
                }
            }
        } else {
            clearbutton.visibility = INVISIBLE
        }

    }

}
