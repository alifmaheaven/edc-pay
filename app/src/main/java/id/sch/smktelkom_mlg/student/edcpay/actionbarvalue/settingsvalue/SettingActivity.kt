package id.sch.smktelkom_mlg.student.edcpay.actionbarvalue.settingsvalue

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.sch.smktelkom_mlg.student.edcpay.R

class SettingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        supportActionBar!!.title = "Setting"

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
