package id.sch.smktelkom_mlg.student.edcpay.walletvalue


import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.sch.smktelkom_mlg.student.edcpay.R
import kotlinx.android.synthetic.main.fragment_wallet3.*
import kotlinx.android.synthetic.main.fragment_wallet3.view.*

class Wallet3Fragment : Fragment() {

    @SuppressLint("ResourceAsColor")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val uangnya2 = activity!!.intent.extras.getString("MESSAGE")
        val uangnya3 = activity!!.intent.extras.getString("MESSAGE2")


        val view = inflater.inflate(R.layout.fragment_wallet3, container, false)

        view.angkadanmatauangnya.text = uangnya2 + " 0.001948"
        view.angkadanmatauangnya2.text = uangnya2 + " 0.001948"
        view.angkadanmatauangnya3.text = uangnya2 + " 0.001948"
        view.buybutton.text = "Buy " + uangnya3
        view.sellbutton.text = "Sell " + uangnya3
        view.sendbutton.text = "Send " + uangnya3
        view.recievebutton.text = "Recieve " + uangnya3

        view.buybutton.setOnClickListener {
            buybutton.setBackgroundColor(Color.parseColor("#3B539B"))
            buybutton.setTextColor(Color.parseColor("#ffffff"))

            sellbutton.setBackgroundColor(Color.parseColor("#ffffff"))
            sellbutton.setTextColor(Color.parseColor("#3B539B"))

            sendbutton.setBackgroundColor(Color.parseColor("#ffffff"))
            sendbutton.setTextColor(Color.parseColor("#3B539B"))

            recievebutton.setBackgroundColor(Color.parseColor("#ffffff"))
            recievebutton.setTextColor(Color.parseColor("#3B539B"))

        }

        view.sellbutton.setOnClickListener {
            sellbutton.setBackgroundColor(Color.parseColor("#3B539B"))
            sellbutton.setTextColor(Color.parseColor("#ffffff"))

            buybutton.setBackgroundColor(Color.parseColor("#ffffff"))
            buybutton.setTextColor(Color.parseColor("#3B539B"))

            sendbutton.setBackgroundColor(Color.parseColor("#ffffff"))
            sendbutton.setTextColor(Color.parseColor("#3B539B"))

            recievebutton.setBackgroundColor(Color.parseColor("#ffffff"))
            recievebutton.setTextColor(Color.parseColor("#3B539B"))

        }

        view.sendbutton.setOnClickListener {
            sendbutton.setBackgroundColor(Color.parseColor("#3B539B"))
            sendbutton.setTextColor(Color.parseColor("#ffffff"))

            sellbutton.setBackgroundColor(Color.parseColor("#ffffff"))
            sellbutton.setTextColor(Color.parseColor("#3B539B"))

            buybutton.setBackgroundColor(Color.parseColor("#ffffff"))
            buybutton.setTextColor(Color.parseColor("#3B539B"))

            recievebutton.setBackgroundColor(Color.parseColor("#ffffff"))
            recievebutton.setTextColor(Color.parseColor("#3B539B"))

        }

        view.recievebutton.setOnClickListener {
            recievebutton.setBackgroundColor(Color.parseColor("#3B539B"))
            recievebutton.setTextColor(Color.parseColor("#ffffff"))

            sellbutton.setBackgroundColor(Color.parseColor("#ffffff"))
            sellbutton.setTextColor(Color.parseColor("#3B539B"))

            buybutton.setBackgroundColor(Color.parseColor("#ffffff"))
            buybutton.setTextColor(Color.parseColor("#3B539B"))

            sendbutton.setBackgroundColor(Color.parseColor("#ffffff"))
            sendbutton.setTextColor(Color.parseColor("#3B539B"))

        }


        return view
    }


}
