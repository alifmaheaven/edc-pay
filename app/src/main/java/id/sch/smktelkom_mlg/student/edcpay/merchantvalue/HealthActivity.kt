package id.sch.smktelkom_mlg.student.edcpay.merchantvalue

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import id.sch.smktelkom_mlg.student.edcpay.R
import id.sch.smktelkom_mlg.student.edcpay.adapter.TabPagerHealthAdapter
import kotlinx.android.synthetic.main.activity_health.*

class HealthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health)


        supportActionBar!!.title = "Cari Health"

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        configureTabLayout()

    }


    private fun configureTabLayout() {


        tab_layout.addTab(tab_layout.newTab().setText("Deals"))
        tab_layout.addTab(tab_layout.newTab().setText("Promo"))


        val adapter = TabPagerHealthAdapter(supportFragmentManager,
                tab_layout.tabCount)
        pager.adapter = adapter

        pager.addOnPageChangeListener(
                TabLayout.TabLayoutOnPageChangeListener(tab_layout))
        tab_layout.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }
}
