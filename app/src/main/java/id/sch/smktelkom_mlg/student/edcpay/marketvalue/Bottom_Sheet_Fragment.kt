package id.sch.smktelkom_mlg.student.edcpay.marketvalue

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.sch.smktelkom_mlg.student.edcpay.R
import kotlinx.android.synthetic.main.fragment_bottom__sheet_.view.*

class Bottom_Sheet_Fragment : BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_bottom__sheet_, container, false)
        view.withdrawtobank.setOnClickListener {
            val intent = Intent(context, Market3Activity::class.java)
            startActivity(intent)
        }

        return view
    }


}
