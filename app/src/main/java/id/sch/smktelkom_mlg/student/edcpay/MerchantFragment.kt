package id.sch.smktelkom_mlg.student.edcpay


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.viewpagerindicator.CirclePageIndicator
import id.sch.smktelkom_mlg.student.edcpay.adapter.ImagesAdapter
import id.sch.smktelkom_mlg.student.edcpay.model.ImagesModel
import kotlinx.android.synthetic.main.fragment_merchant.view.*
import java.util.*


class MerchantFragment : Fragment() {


    private lateinit var v: View

    private var imageModelArrayList: ArrayList<ImagesModel>? = null

    private val myImageList = intArrayOf(
            R.drawable.promodua1,
            R.drawable.promodua3,
            R.drawable.promodua4

    )



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_merchant, container, false)

        v.buttonHealth.setOnClickListener {
            val navControler = Navigation.findNavController(v)
            navControler.navigate(R.id.action_navigation_merchant_to_healthActivity)
        }


        v.nearmebutton.setOnClickListener {
            val navControler = Navigation.findNavController(v)
            navControler.navigate(R.id.action_navigation_merchant_to_nearMeActivity)
        }

        v.buttonvoucher.setOnClickListener {
            val navControler = Navigation.findNavController(v)
            navControler.navigate(R.id.action_navigation_merchant_to_voucherActivity)
        }

        v.buttonmall.setOnClickListener {
            val navControler = Navigation.findNavController(v)
            navControler.navigate(R.id.action_navigation_merchant_to_mallActivity)
        }


        imageModelArrayList = ArrayList()
        imageModelArrayList = populateList()


        init()


        return v
    }

    private fun populateList(): ArrayList<ImagesModel> {

        val list = ArrayList<ImagesModel>()

        for (i in 0..2) {
            val imageModel = ImagesModel()
            imageModel.setImageDrawables(myImageList[i])
            list.add(imageModel)
        }

        return list
    }

    private fun init() {

        mPager = v.findViewById(R.id.pager2)
        mPager!!.adapter = ImagesAdapter((this.activity as Context?)!!, this.imageModelArrayList!!)

        val indicator = v.findViewById<CirclePageIndicator>(R.id.indicator2)

        indicator.setViewPager(mPager)

        val density = resources.displayMetrics.density

        //Set circle indicator radius
        indicator.radius = 5 * density

        NUM_PAGES = imageModelArrayList!!.size

        // Auto start of viewpager
        val handler = Handler()
        val update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            mPager!!.setCurrentItem(currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 3000, 3000)

        // Pager listener over indicator
        indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position

            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {

            }
        })

    }

    companion object {

        @SuppressLint("StaticFieldLeak")
        private var mPager: ViewPager? = null
        private var currentPage = 0
        private var NUM_PAGES = 0
    }





}
