package id.sch.smktelkom_mlg.student.edcpay


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import id.sch.smktelkom_mlg.student.edcpay.marketvalue.Bottom_Sheet_Fragment
import id.sch.smktelkom_mlg.student.edcpay.marketvalue.SmartContactActivity
import kotlinx.android.synthetic.main.fragment_market.view.*


class MarketFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_market, container, false)
        view.buttonbuy.setOnClickListener {
            val navControler = Navigation.findNavController(view)
            navControler.navigate(R.id.action_navigation_market_to_marketActivity)
        }

        view.smartcontractbutton.setOnClickListener {
            val intent = Intent(context, SmartContactActivity::class.java)
            startActivity(intent)
        }

        view.withdrawButton.setOnClickListener {
            val bottomSheetFragment = Bottom_Sheet_Fragment()
            bottomSheetFragment.show(childFragmentManager, bottomSheetFragment.tag)
        }

        return view
    }


}
