package id.sch.smktelkom_mlg.student.edcpay.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import id.sch.smktelkom_mlg.student.edcpay.walletvalue.Wallet3Fragment
import id.sch.smktelkom_mlg.student.edcpay.walletvalue.Wallet4Fragment


class TabPagerWalletAdapter(fm: FragmentManager, private var tabCount: Int) :
        FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {

        when (position) {
            0 -> return Wallet3Fragment()
            1 -> return Wallet4Fragment()

            else -> return null
        }
    }

    override fun getCount(): Int {
        return tabCount
    }
}