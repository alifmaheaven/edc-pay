package id.sch.smktelkom_mlg.student.edcpay.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import id.sch.smktelkom_mlg.student.edcpay.merchantvalue.Nearme1Fragment
import id.sch.smktelkom_mlg.student.edcpay.merchantvalue.Nearme2Fragment

class TabPagerNearmeAdapter(fm: FragmentManager, private var tabCount: Int) :
        FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {

        when (position) {
            0 -> return Nearme1Fragment()
            1 -> return Nearme2Fragment()

            else -> return null
        }
    }

    override fun getCount(): Int {
        return tabCount
    }
}