package id.sch.smktelkom_mlg.student.edcpay

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import id.sch.smktelkom_mlg.student.edcpay.actionbarvalue.notificationvalue.NotificationActivity
import id.sch.smktelkom_mlg.student.edcpay.actionbarvalue.settingsvalue.SettingActivity
import id.sch.smktelkom_mlg.student.edcpay.bottomnavigationvalue.ScanActivity
import id.sch.smktelkom_mlg.student.edcpay.bottomnavigationvalue.request.Request2Activity
import id.sch.smktelkom_mlg.student.edcpay.bottomnavigationvalue.send.Send2Activity
import id.sch.smktelkom_mlg.student.edcpay.bottomnavigationvalue.topup.TopupActivity
import kotlinx.android.synthetic.main.activity_main.*
import me.dm7.barcodescanner.zbar.ZBarScannerView


class MainActivity : AppCompatActivity() {

    private lateinit var mScannerView: ZBarScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)







        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        topNavigationView.setOnNavigationItemSelectedListener { item ->

            NavigationUI.onNavDestinationSelected(item, Navigation.findNavController(this, R.id.my_nav_host_fragment))

        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {

        R.id.action_settings -> {
            // User chose the "Print" item
            val intent = Intent(this, SettingActivity::class.java)
            startActivity(intent)
            true
        }

        R.id.action_notification -> {
            // User chose the "Print" item
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }

    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.barcode -> {
                val settingsDialog = Dialog(this)
                settingsDialog.window.requestFeature(Window.FEATURE_NO_TITLE)
                settingsDialog.setContentView(layoutInflater.inflate(R.layout.barcode_popup, null))
                settingsDialog.show()

                return@OnNavigationItemSelectedListener true

            }
            R.id.scan -> {
                val intent = Intent(this, ScanActivity::class.java)
                startActivity(intent)
                return@OnNavigationItemSelectedListener true

            }

            R.id.send -> {
                val intent = Intent(this, Send2Activity::class.java)
                startActivity(intent)
                return@OnNavigationItemSelectedListener true

            }

            R.id.request -> {
                val intent = Intent(this, Request2Activity::class.java)
                startActivity(intent)
                return@OnNavigationItemSelectedListener true

            }
            R.id.topup -> {
                val intent = Intent(this, TopupActivity::class.java)
                startActivity(intent)
                return@OnNavigationItemSelectedListener true

            }

        }
        false
    }


}






