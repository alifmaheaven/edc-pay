package id.sch.smktelkom_mlg.student.edcpay.merchantvalue


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.sch.smktelkom_mlg.student.edcpay.R

class Nearme1Fragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_nearme1, container, false)
    }


}
