package id.sch.smktelkom_mlg.student.edcpay.actionbarvalue.settingsvalue


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation

import id.sch.smktelkom_mlg.student.edcpay.R
import kotlinx.android.synthetic.main.fragment_setting2.view.*

class Setting2Fragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_setting2, container, false)

        view.buttonubahnomor.setOnClickListener {
            val navControler = Navigation.findNavController(view)
            navControler.navigate(R.id.action_setting2Fragment_to_setting3Fragment)
        }

        view.buttonubahemail.setOnClickListener {
            val navControler = Navigation.findNavController(view)
            navControler.navigate(R.id.action_setting2Fragment_to_setting4Fragment)
        }



        return view
    }


}
