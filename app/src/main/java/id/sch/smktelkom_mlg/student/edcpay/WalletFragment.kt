package id.sch.smktelkom_mlg.student.edcpay

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.sch.smktelkom_mlg.student.edcpay.walletvalue.BuyWalletActivity
import id.sch.smktelkom_mlg.student.edcpay.walletvalue.SellWalletActivity
import id.sch.smktelkom_mlg.student.edcpay.walletvalue.Wallet2Activity
import id.sch.smktelkom_mlg.student.edcpay.walletvalue.WithdrawWalletActivity
import kotlinx.android.synthetic.main.fragment_wallet.view.*

val EXTRA_MESSAGE = "MESSAGE"
val EXTRA_MESSAGE2 = "MESSAGE2"

class WalletFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_wallet, container, false)


        view.walletbuttondetailETH.setOnClickListener {
            val intent = Intent(container!!.context, Wallet2Activity::class.java)
            intent.putExtra(EXTRA_MESSAGE2, "Ether")
            intent.putExtra(EXTRA_MESSAGE, "ETH")
            startActivity(intent)
        }
        view.walletbuttondetailIDR.setOnClickListener {
            val intent = Intent(container!!.context, Wallet2Activity::class.java)
            intent.putExtra(EXTRA_MESSAGE2, "Rupiah")
            intent.putExtra(EXTRA_MESSAGE, "IDR")
            startActivity(intent)
        }

        view.sellbutton.setOnClickListener {
            val intent = Intent(container!!.context, SellWalletActivity::class.java)
            startActivity(intent)
        }

        view.buybutton.setOnClickListener {
            val intent = Intent(container!!.context, BuyWalletActivity::class.java)
            startActivity(intent)
        }

        view.withdrawbuttonidr.setOnClickListener {
            val intent = Intent(container!!.context, WithdrawWalletActivity::class.java)
            startActivity(intent)
        }
        return view
    }


}
