package id.sch.smktelkom_mlg.student.edcpay.bottomnavigationvalue.topup

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import id.sch.smktelkom_mlg.student.edcpay.R
import kotlinx.android.synthetic.main.activity_topup.*

class TopupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_topup)

        supportActionBar!!.title = "Top Up"

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        buttontopup.setOnClickListener {
            Toast.makeText(this, "Silahkan Cek Email anda", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }
}
