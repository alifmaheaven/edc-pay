package id.sch.smktelkom_mlg.student.edcpay.merchantvalue

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.sch.smktelkom_mlg.student.edcpay.R
import kotlinx.android.synthetic.main.activity_mall.*

class MallActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mall)
        supportActionBar!!.title = "Mall"
        // supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#FFFFFFFF")))
        // supportActionBar!!.title = (Html.fromHtml("<font color=\"#000000\">" + "Mall" + "</font>"))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        buttonmall1.setOnClickListener {
            val intent = Intent(this, Mall2Activity::class.java)
            startActivity(intent)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }
}
