package id.sch.smktelkom_mlg.student.edcpay.model

class ImagesModel {
    private var imageDrawable: Int = 0

    fun getImageDrawables(): Int {
        return imageDrawable
    }

    fun setImageDrawables(image_drawable: Int) {
        this.imageDrawable = image_drawable
    }
}