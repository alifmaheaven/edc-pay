package id.sch.smktelkom_mlg.student.edcpay.walletvalue

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import id.sch.smktelkom_mlg.student.edcpay.R
import id.sch.smktelkom_mlg.student.edcpay.adapter.TabPagerWalletAdapter
import kotlinx.android.synthetic.main.activity_wallet2.*


class Wallet2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet2)

        val uangnya: String = intent.getStringExtra("MESSAGE")

        supportActionBar!!.title = uangnya + " Wallet"

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        dompetmatauangnya.text = uangnya + " Wallet"

        configureTabLayout()
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }



    private fun configureTabLayout() {

        tab_layout2.addTab(tab_layout2.newTab().setText("OVERVIEW"))
        tab_layout2.addTab(tab_layout2.newTab().setText("TRANSACTIONS"))


        val adapter = TabPagerWalletAdapter(supportFragmentManager,
                tab_layout2.tabCount)

        pager2.adapter = adapter

        pager2.addOnPageChangeListener(
                TabLayout.TabLayoutOnPageChangeListener(tab_layout2))
        tab_layout2.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                pager2.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })
    }

    /*private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_market -> {

                onBackPressed()
                finish()

                return@OnNavigationItemSelectedListener true

            }

            R.id.navigation_home -> {
                onBackPressed()
                finish()

                return@OnNavigationItemSelectedListener true

            }
            R.id.navigation_merchant -> {
                onBackPressed()
                finish()

                return@OnNavigationItemSelectedListener true

            }
            R.id.navigation_wallet -> {

                return@OnNavigationItemSelectedListener true

            }
            R.id.navigation_history -> {
                onBackPressed()
                finish()

                return@OnNavigationItemSelectedListener true

            }

        }
        false
    }*/
}
