package id.sch.smktelkom_mlg.student.edcpay.merchantvalue

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.sch.smktelkom_mlg.student.edcpay.R

class VoucherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voucher)

        supportActionBar!!.title = "Voucher"

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }
}
