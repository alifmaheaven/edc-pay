package id.sch.smktelkom_mlg.student.edcpay.bottomnavigationvalue.send

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.sch.smktelkom_mlg.student.edcpay.R
import kotlinx.android.synthetic.main.activity_send.*

class SendActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send)

        nextbutton.setOnClickListener {
            val intent = Intent(this, Send2Activity::class.java)
            startActivity(intent)
        }

        supportActionBar!!.title = "Send"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }
}
