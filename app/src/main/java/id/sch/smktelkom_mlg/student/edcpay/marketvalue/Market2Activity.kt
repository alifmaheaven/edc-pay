package id.sch.smktelkom_mlg.student.edcpay.marketvalue

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.sch.smktelkom_mlg.student.edcpay.R

class Market2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_market2)
        supportActionBar!!.title = "Amount"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }
}
